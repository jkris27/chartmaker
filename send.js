const axios = require('axios');
const p = require('promisify-node');
const fs = p('fs');
const zapier_url = 'https://hooks.zapier.com/hooks/catch/605246/rv7k12/';

fs.readFile('./testimage.png').
   then(file => {
      axios({
         method: 'post',
         url: zapier_url,
         data: file
      }).
      then(console.log).
      catch(console.error);
   }).
   catch(console.error)
