const axios = require('axios');

const jkris_wh = 'https://hooks.slack.com/services/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';
const _analytics_wh = 'https://hooks.slack.com/services/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'

module.exports = function main(imgur_link, month) {
   axios.post(_analytics_wh,
       {
          text: 'Sirious Analytics Service.\r\b Can\'t get anymore serious than this.',
          attachments: [
             {
                pretext:':chart_with_upwards_trend: Google Analytics',
                color: 'good',
                text: 'Can\'t view the image? '+imgur_link,
                author_name:'Total Page Views/Day',
                image_url: imgur_link,
                footer: 'Sirious API'
             },
             {
                fields: [
                   {
                      title: 'Month',
                      value: 'September',
                      short: 'true'
                   },
                   {
                      title: 'Page',
                      value: '"/" (All)',
                      short: 'true'
                   }
                ]
             }
          ]
       }
   ).
   then(response => console.log('Succesfully delivered')).
   catch(error => console.error('Error', error));
}
