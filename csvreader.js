const csv = require('fast-csv');

module.exports = function(callback){
   let rs = [];
   csv.
      fromPath('./ga/data.csv', {headers:true}).
      on('data', data => {
         rs.push(data);
      }).
      on('end', _=> {
         callback(rs);
      });
}

