const CJS = require('chartjs-node');
const csvr = require('./csvreader');

const SAVE_PATH = './imgur/upload.jpg';

let chartNode = new CJS(800, 500);

module.exports = function main_chartjs(callback) {
   csvr(data => {
      let labels = [];
      let dataset1 = {
         label: 'Sessions',
         borderColor: '#f44336',
         backgroundColor: '#f44336',
         data: []
      };
      let dataset2 = {
         label: 'Pageviews',
         borderColor: '#2196f3',
         backgroundColor: '#2196f3',
         data: []
      };

      data.forEach(d => {
         labels.push(d['ga:day']);
         dataset1.data.push(d['ga:sessions']); 
         dataset2.data.push(d['ga:pageviews']); 
      });

      const options = getDefaultOptions({labels, datasets: [dataset1, dataset2]});

      chartNode.
         drawChart(options).
         then(buffer => chartNode.getImageStream('image/jpg')).
         then(streamRs => {
            chartNode.writeImageToFile('image/jpg', SAVE_PATH);
            callback();               
         });
   })
}

function getDefaultOptions({ labels, datasets } = params) {
   datasets.forEach(d => {
      Object.assign(d, {
         fill: false,
         borderWidth: 1
      });
   });

   return {
       type: 'line',
       data: {
           labels,
           datasets
       },
       options: {
           scales: {
               yAxes: [{
                   ticks: {
                       beginAtZero:false
                   }
               }]
           },
          plugins: {
             beforeDraw: cjs => {
                var ctx = cjs.chart.ctx;
                ctx.fillStyle = "white";
                ctx.fillRect(0, 0, cjs.chart.width, cjs.chart.height);
             }
          }
       }
   };
}

