import os
from imgurpython import ImgurClient
from imgurpython.helpers.error import ImgurClientError

client_id = 'c17adf855326bda'
client_secret = '87d7e8f78edae15043aba023a25c784086400b28'

client = ImgurClient(client_id, client_secret)

def here():
  return os.path.dirname(os.path.realpath(__file__))

try:
    rs = client.upload_from_path(here()+'/upload.jpg', config=None, anon=True)
    link = rs.get('link');
    print link
except ImgurClientError as e:
    print(e.error_message)
    print(e.status_code)
