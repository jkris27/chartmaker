const cmd = require('node-cmd');
const stdcallback = fn => (err,data,stderr) => {
   if (err) console.error(err);
   if (stderr) console.error(stderr);
   fn(data);
}
// Sample Usage: cmd.get('pwd', stdcallback(data => console.log(data)));

// Chartmaker JS
// ------------
// Execute 
// 1. Cron job 
// 2. Web hook [ future ]
//
// Possible parameters
// 1. All website data for month
// 2. Data group by Catagories
// 3. Individual articles

const chart = require('./chart.js');
const slack = require('./slack.js');

main();

function main() {
   // Trigger get analytics 
   cmd.get('python ./ga/LastMonthAll.py', stdcallback(_gaComplete));
}

function _gaComplete() {
   chart(_chartComplete);
}

function _chartComplete() {
   cmd.get('python ./imgur/main.py', stdcallback(_imgurComplete));
}

function _imgurComplete(link) {
   slack(link);
}
