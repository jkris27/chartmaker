"""Hello Analytics Reporting API V4."""

import csv
import os

from apiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials
import datetime

def here():
  return os.path.dirname(os.path.realpath(__file__))
  
def get_lastmonth_range():
  today = datetime.date.today()
  first = today.replace(day=1)
  lastMonth = first - datetime.timedelta(days=1)
  strFrom = lastMonth.strftime('%Y-%m-01')
  strTo = lastMonth.strftime('%Y-%m-%d')
  return strFrom, strTo

SCOPES = ['https://www.googleapis.com/auth/analytics.readonly']
KEY_FILE_LOCATION = here()+'/sgwmscog-ac6f2a48aa50.json'
VIEW_ID = '131837968'

print KEY_FILE_LOCATION

def initialize_analyticsreporting():
  """Initializes an Analytics Reporting API V4 service object.

  Returns:
    An authorized Analytics Reporting API V4 service object.
  """
  credentials = ServiceAccountCredentials.from_json_keyfile_name(
      KEY_FILE_LOCATION, SCOPES)

  # Build the service object.
  analytics = build('analyticsreporting', 'v4', credentials=credentials)

  return analytics


def get_report(analytics):
  """Queries the Analytics Reporting API V4.

  Args:
    analytics: An authorized Analytics Reporting API V4 service object.
  Returns:
    The Analytics Reporting API V4 response.
  """
  dateFrom, dateTo = get_lastmonth_range()
  return analytics.reports().batchGet(
      body={
        'reportRequests': [
        {
          'viewId': VIEW_ID,
          'dateRanges': [{'startDate': dateFrom, 'endDate': dateTo}],
          'metrics': [{'expression': 'ga:sessions'},{'expression': 'ga:pageviews'}],
          'dimensions': [{'name': 'ga:pagePath'},{'name': 'ga:day'}],
          'dimensionFilterClauses': [
              {
                  'filters': [
                      {
                          'dimensionName': 'ga:pagePath',
                          'operator': 'EXACT',
                          'expressions': ['/']
                      }
                  ]
              }
          ],
          'orderBys': [ {'fieldName': 'ga:day'} ]
        }]
      }
  ).execute()


def print_response(response):
  """Parses and prints the Analytics Reporting API V4 response.

  Args:
    response: An Analytics Reporting API V4 response.
  """
  for report in response.get('reports', []):
    columnHeader = report.get('columnHeader', {})
    dimensionHeaders = columnHeader.get('dimensions', [])
    metricHeaders = columnHeader.get('metricHeader', {}).get('metricHeaderEntries', [])

    for row in report.get('data', {}).get('rows', []):
      dimensions = row.get('dimensions', [])
      dateRangeValues = row.get('metrics', [])

      for header, dimension in zip(dimensionHeaders, dimensions):
        print header + ': ' + dimension

      for i, values in enumerate(dateRangeValues):
        print 'Date range: ' + str(i)
        for metricHeader, value in zip(metricHeaders, values.get('values')):
          print metricHeader.get('name') + ': ' + value

def print_response2(response):
  """Parses and prints the Analytics Reporting API V4 response.

  Args:
    response: An Analytics Reporting API V4 response.
  """
  for report in response.get('reports', []):
    columnHeader = report.get('columnHeader', {})
    dimensionHeaders = columnHeader.get('dimensions', [])
    metricHeaders = columnHeader.get('metricHeader', {}).get('metricHeaderEntries', [])

    hrows = []
    for header in dimensionHeaders:
        hrows.append(header)
    for header in metricHeaders:
        hrows.append(header.get('name'))

    arows = []
    arows.append(hrows)
    for row in report.get('data', {}).get('rows', []):
      trow = []
      dimensions = row.get('dimensions', [])
      dateRangeValues = row.get('metrics', [])

      for dvalue in dimensions:
        trow.append(dvalue)

      for mvalue in dateRangeValues[0].get('values'):
        trow.append(mvalue)

      arows.append(trow)

    with open(here()+'/data.csv', 'w') as wf:
        write=csv.writer(wf)
        write.writerows(arows)

def main():
  analytics = initialize_analyticsreporting()
  response = get_report(analytics)
  print_response2(response)

if __name__ == '__main__':
  main()
